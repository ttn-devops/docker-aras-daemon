#!/bin/bash
TZ=${TIMEZONE:-"Etc/UTC"}
ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

if [ ${REALTIME:-false} == true ]; then
  REALTIME_OPT=-R
else
  REALTIME_OPT=-r
fi

if [ ${OVERRIDE_CONF:-false} == true ]; then
    echo "Overriding conf!"
    cp /aras/aras.conf /etc/aras/aras.conf
fi

if [ ! -f /etc/aras/aras.conf ]; then
    cp /aras/aras.conf /etc/aras/aras.conf
fi

if [ ! -f /etc/aras/aras.block ]; then
    cp /aras/aras.block /etc/aras/aras.block
fi

if [ ! -f /etc/aras/aras.schedule ]; then
    cp /aras/aras.schedule /etc/aras/aras.schedule
fi

if [ ! -f /etc/aras/jack.plumbing ]; then
    cp /aras/jack.plumbing /etc/aras/jack.plumbing
fi

sed -i 's/SCHEDULE_FILE/'"${SCHEDULE_FILE:-\/etc\/aras\/aras.schedule}"'/' /etc/aras/aras.conf
sed -i 's/BLOCK_FILE/'"${BLOCK_FILE:-\/etc\/aras\/aras.block}"'/' /etc/aras/aras.conf
sed -i 's/LOG_FILE/'"${LOG_FILE:-\/dev\/stdout}"'/' /etc/aras/aras.conf
sed -i 's/CONFIGURATION_PERIOD/'"${CONFIGURATION_PERIOD:-10000}"'/' /etc/aras/aras.conf
sed -i 's/ENGINE_PERIOD/'"${ENGINE_PERIOD:-100}"'/' /etc/aras/aras.conf
sed -i 's/DEFAULT_BLOCK_MODE/'"${DEFAULT_BLOCK_MODE:-on}"'/' /etc/aras/aras.conf
sed -i 's/DEFAULT_BLOCK/'"${DEFAULT_BLOCK:-default}"'/' /etc/aras/aras.conf
sed -i 's/SCHEDULE_MODE/'"${SCHEDULE_MODE:-hard}"'/' /etc/aras/aras.conf
sed -i 's/TIME_SIGNAL_BLOCK/'"${TIME_SIGNAL_BLOCK:-time_signal}"'/' /etc/aras/aras.conf
sed -i 's/BLOCK_PLAYER_NAME/'"${BLOCK_PLAYER_NAME:-block_player}"'/' /etc/aras/aras.conf
sed -i 's/BLOCK_PLAYER_CHANNELS/'"${BLOCK_PLAYER_CHANNELS:-2}"'/' /etc/aras/aras.conf
sed -i 's/BLOCK_PLAYER_SAMPLE_RATE/'"${BLOCK_PLAYER_SAMPLE_RATE:-44100}"'/' /etc/aras/aras.conf
sed -i 's/BLOCK_PLAYER_VOLUME/'"${BLOCK_PLAYER_VOLUME:-0.5}"'/' /etc/aras/aras.conf
sed -i 's/BLOCK_PLAYER_AUDIO_OUTPUT/'"${BLOCK_PLAYER_AUDIO_OUTPUT:-jack}"'/' /etc/aras/aras.conf
sed -i 's/BLOCK_PLAYER_AUDIO_DEVICE/'"${BLOCK_PLAYER_AUDIO_DEVICE:-default}"'/' /etc/aras/aras.conf
sed -i 's/TIME_SIGNAL_PLAYER_NAME/'"${TIME_SIGNAL_PLAYER_NAME:-time_signal_player}"'/' /etc/aras/aras.conf
sed -i 's/TIME_SIGNAL_PLAYER_CHANNELS/'"${TIME_SIGNAL_PLAYER_CHANNELS:-2}"'/' /etc/aras/aras.conf
sed -i 's/TIME_SIGNAL_PLAYER_SAMPLE_RATE/'"${TIME_SIGNAL_PLAYER_SAMPLE_RATE:-44100}"'/' /etc/aras/aras.conf
sed -i 's/TIME_SIGNAL_PLAYER_VOLUME/'"${TIME_SIGNAL_PLAYER_VOLUME:-0.5}"'/' /etc/aras/aras.conf
sed -i 's/TIME_SIGNAL_PLAYER_AUDIO_OUTPUT/'"${TIME_SIGNAL_PLAYER_AUDIO_OUTPUT:-jack}"'/' /etc/aras/aras.conf
sed -i 's/TIME_SIGNAL_PLAYER_AUDIO_DEVICE/'"${TIME_SIGNAL_PLAYER_AUDIO_DEVICE:-default}"'/' /etc/aras/aras.conf
sed -i 's/FADE_OUT_TIME/'"${FADE_OUT_TIME:-2000}"'/' /etc/aras/aras.conf
sed -i 's/FADE_OUT_SLOPE/'"${FADE_OUT_SLOPE:-0.2}"'/' /etc/aras/aras.conf
sed -i 's/TIME_SIGNAL_MODE/'"${TIME_SIGNAL_MODE:-off}"'/' /etc/aras/aras.conf
sed -i 's/TIME_SIGNAL_ADVANCE/'"${TIME_SIGNAL_ADVANCE:-4000}"'/' /etc/aras/aras.conf

echo "jackd $REALTIME_OPT -d net -a ${JACK_SERVER:-'127.0.0.1'} -n ${CLIENT_NAME:-'aras-daemon'} 1> /dev/null 2> /dev/null &"
jackd $REALTIME_OPT -d net -a ${JACK_SERVER:-'127.0.0.1'} -n ${CLIENT_NAME:-'aras-daemon'} 1> /dev/null 2> /dev/null &
sleep ${JACK_TIMEOUT:-5}
jack-plumbing -q -o -u 100000 ${PLUMBING_FILE:-\/etc\/aras\/jack.plumbing} 2> /dev/null &
exec aras-daemon ${CONFIG_FILE:-\/etc\/aras\/aras.conf}
