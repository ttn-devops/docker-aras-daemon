FROM registry.gitlab.com/ttn-devops/docker-jackd

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
    tzdata \
    wget \
    libgstreamer1.0-0 \
    gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-good \
    gstreamer1.0-plugins-ugly

RUN mkdir -p /tmp/aras cd /tmp/aras && \
    wget -q https://downloads.sourceforge.net/project/aras/aras/ARAS-4.4/aras-common_4.4-1_all.deb -O aras_common.deb && \
    wget -q https://downloads.sourceforge.net/project/aras/aras/ARAS-4.4/aras-daemon_4.4-1_amd64.deb -O aras_daemon.deb && \
    dpkg -i aras_common.deb && \
    dpkg -i aras_daemon.deb && \
    rm -rf /tmp/aras /etc/aras && \
    mkdir -p /etc/aras

COPY aras /aras
COPY start.sh /usr/local/bin/start.sh
VOLUME /etc/aras

CMD ["start.sh"]
